Subject: Release Update: arch status, major transitions finished, freeze coming up
To: debian-devel-announce@lists.debian.org

Hi,

Once again, it's time to review the current state of lenny. The last few
weeks have been rather busy with major transitions, so this mail is a
bit late.


Architecture status
~~~~~~~~~~~~~~~~~~~
The status of Debian on the armel architecture has continued to improve,
so we have decided to upgrade it to a proper release architecture.
The architecture qualification pages on wiki.debian.org are still
missing a LOT of information, so please put in a concerted effort to
ensure that these are completed shortly.


Major transitions
~~~~~~~~~~~~~~~~~
Since our last update, we have upgraded the python and perl versions to
the latest current upstream versions. These upgrades were the last major
changes to the base system for lenny. Also, updates of ocaml and gcc-defaults
have successfuly happened. A transition of xulrunner is still pending,
however this will be one of the last (or the last) larger transitions.


Release goals
~~~~~~~~~~~~~
* Support for future gcc versions

gcc-4.3 will be the default on all architectures in the near future. Only
a few bugs are left in lenny and most of these are already fixed in 
unstable. We consider the work for this release goal to be successfully
completed.

* Switch /bin/sh to dash

There are still quite a few bugs open about bashisms, but most of those
have a patch included. Please NMU. This goal doesn't mean we'd switch to
dash as a global default, but if people do so on their system, there
shouldn't be any issue after the last bugs have been finished off.

* piuparts-clean archive

Over 50 bugs remaining, many with little activity.  Since these are
problems that affect all users and are usually fixed by little changes to
the maintainer scripts, more attention to this goal would be very
welcome.

* double compilation support

Most of the double compilation problems have been fixed since the last
release update and there are only about three dozen bugs left. Please note
that many of the packages still affected are in bad general shape, so each
NMUer should consider if the package in question shouldn't be removed
instead.

* Prepare init.d-Scripts for dependency-based init systems

Wider testing of dependency-based init systems has lead to some new bugs
for this goal, but the current state looks quite well. We are confident
that we will have full support for dep-based init system in lenny.

* I18n support in all debconf-using packages

Finished. Yay.

* Support for python2.5

Finished. Yay.


Removals from testing
~~~~~~~~~~~~~~~~~~~~~
There have recently been some raised eyebrows about removals from
testing. The release team has a clear policy in the matter: packages with RC
bugs older than 20 days, with no activity from the maintainer are reason
enough for a removal from testing. Only packages that are leaf nodes in the
dependency tree get removed this way.

This does not mean (yet) that your package will not be shipped with lenny, a
new upload with the offending bugs fixed, and the package will transition like
any other.


BSP Marathon
~~~~~~~~~~~~
At time of writing, we have 480 open RC bugs, which is 480 too many.  A
coordinated effort is needed to reduce this number, so we've decided to
resurrect last year's very successful BSP marathons. As a reminder, we
still have a 0-day NMU policy in effect.

Please note that in a BSP, you shouldn't just NMU every RC bug you see.
While you are working on a package, check for other low-hanging fruits
(like translation updates, typos that can easily be fixed, ...) and fix
them in your NMU. On the other hand, if you notice that a package looks
unmaintained, refrain from fixing the bugs for now and try to find out if
the package should be removed or adopted by another maintainer instead.

To give our BSPs a more targetted feeling, we want to assign one group of
RC bugs and one release goal to each weekend:

BSP on weekend 2008-06-14 to 2008-06-15 in Utrecht, the Netherlands [BSP:NL]
-------------------------------------------------------------------
 + Fix remaining FTBFS bugs
 + Fix remaining problems with dash as /bin/sh [RG:D]

BSP on weekend 2008-06-28 to 2008-06-29 in Cambridge, UK [BSP:UK]
--------------------------------------------------------
 + Check debian-installer and the installation process
 + Fix piuparts problems [RG:P]


Release schedule
~~~~~~~~~~~~~~~~
Though the number of release critical bugs concerns us, we are pleased
by the overall state of lenny. Most big software packages have been
updated to the major version that will be shipped in lenny, so we will
be able to concentrate on polishing in the following months:

Early of June 2008
  Freeze of the non-essential toolchain
    The "non-essential toolchain" means things like debhelper, cdbs
    and a big chunk of other things usually needed to produce binary
    packages.

End of June 2008
  Freeze of all library packages
    This will affect all packages that produce library packages used
	by other packaged software. Packages without r-deps won't be
	frozen at this point.

Mid of July 2008
  Full freeze
    Please don't wait with uploads for the last day before the freeze,
    thanks.

September 2008
  Release lenny!


Package team news
~~~~~~~~~~~~~~~~~

 * The GNOME team has decided to delay the upload of nautilus 2.22 (and
   several other similar packages) to unstable. GNOME 2.22 introduced
   the new gvfs library, a replacement for the venerable gnome-vfs,
   which is, while introducing several new features, not yet completely
   regression-free. More complex file backends, such as smb, are still not
   ready for mass-deployment.
 * The KDE team is continuing to prepare packages for KDE4.1 development
   releases. The first beta has just been uploaded to experimental and
   user are encouraged to test it [KDE41]. Please note that we haven't
   decided yet on the inclusion of KDE4.1 in lenny, but plan to do so
   in the near future.
 * Iceweasel/Firefox and other Mozilla stuff: A transition to the new
   xulrunner version 1.9 is ahead. xulrunner 1.9 is another word for
   iceweasel 3, so massive changes planned. [XUL]


Freeze coming up
~~~~~~~~~~~~~~~~
We are going to freeze the complete toolchain and all libs VERY SOON. This
means that we need your help in ensuring a smooth release process. If you
have a new library that is needed for lenny, do not wait a month to upload
it, as it won't make it in time. However, please don't immediately upload
either. Send debian-release a mail, and prepare packages in experimental.
Now is the time to think about and prepare those final versions of these
libraries.


Tricks from the Release Team
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The tool that sends mail to package maintainers when their package is removed
from testing [TRILLE] don't know about all our recently added members. That's
the reason why you don't always have the name of the Release Team member that
asked for your package removal in that mail. This is being worked on right
now. Until then you can consult Release Team members' hint files directly on 
our webpage [HINTS].

Cheers,
NN
-- 
http://release.debian.org
Debian Release Team

References:
 [RG:D] http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-release@lists.debian.org&tag=goal-dash
 [RG:P]   http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-release@lists.debian.org&tag=piuparts-stable-upgrade&dist=testing
 [KDE41] http://ekaia.org/blog/2008/05/29/how-to-install-kde-4-beta1-from-experimental/
 [XUL] http://lists.debian.org/debian-release/2008/05/msg00009.html
 [TRILLE] http://people.debian.org/~henning/trille/
 [HINTS] http://release.debian.org/britney/hints/
 [BSP:NL] http://wiki.debian.org/BSP2008/Utrecht
 [BSP:UK] http://wiki.debian.org/BSP2008/CambridgeJune
