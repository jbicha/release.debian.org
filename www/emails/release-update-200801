Subject: release update: Release team, blockers, architectures, schedule, goals
To: debian-devel-announce@lists.debian.org

Hi,

Some time has passed since our last release update and there are some
important points to communicate in order to get lenny in shape for
release.

New Release Assistants
~~~~~~~~~~~~~~~~~~~~~~
We are pleased to announce the addition of three new members to the release
team; Pierre Habouzit (madcoder), Philipp Kern (pkern) and Neil McGovern
(neilm) have joined the team as release assistants and will help out with
needed work in all areas.

Release blockers
~~~~~~~~~~~~~~~~
We haven't identified any additional release blockers and don't expect to
identify any more. Therefore we regard the list of Release Blockers
as frozen.
Note that this doesn't preclude adding more Release Goals [0]. As
indicated before, the main difference between Release Blockers and Goals
is that we won't delay the release when a Release Goal has not been
reached.

Release architecture re-qualification
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
As our previous call [1] for updates for the release architecture
re-qualification process was not really a success, we recently resent the
call to the porters lists [2] and hope this will be act as a final
reminder to complete the wiki pages [3].

At the moment, we are still missing some information for the alpha and
hppa architectures.

We have also been considering the inclusion of armel for the set of
architectures to be released with lenny. Its inclusion in the main
ftp archive is being worked on at the moment.

Release schedule
~~~~~~~~~~~~~~~~
As we are progressing in our release preparations; we have reviewed
the original schedule for lenny to check for any imminent problems, and
at the moment are quite content with the current state. We are, as always,
concerned about the large number of release critical issues still unfixed
in testing, so please help do something about it (see below for ideas :P)

Now on to the actual, not much revised, schedule:

Early March 2008
  Very soft freeze
    Please start thinking about the release when uploading new major
	upstream versions. Only upload to unstable if you are sure that
	the software will be stable before we release. If you are not
	convinced, use experimental as staging area.

  Freeze of release goal list
    We will announce the final list of release goals and report about
    the progress made in each area. At this point, goals which look
    too hard to complete for lenny will be removed from the list (and
    automatically put on the list for lenny+1)

  Start of the second BSP marathon for Lenny
    See below for more information about this, but you can and should
    help with it.

Early April 2008
  Freeze of the essential toolchain

Mid of June 2008
  Freeze of the non-essential toolchain and all libraries
    The "non-essential toolchain" means things like debhelper, cdbs 
    and a big chunk of other things usually needed to produce binary
    packages.

Mid of July 2008
  Full freeze
    Please don't wait with uploads for the last day before the freeze,
    thanks.

September 2008
  Release lenny!


BSP Marathon
~~~~~~~~~~~~
At time of writing, we have 460 open RC bugs, which is 460 too many.  A
coordinated effort is needed to reduce this number, so we've decided to
resurrect last year's very successful BSP marathons. As a reminder, we
still have a 0-day NMU policy in effect[4].
Why not join in, and you too can be a bug remover with wax! [5]

The BSPs we're planning will be focused on some sub-systems, so to help to
release Lenny, *you* need to fix RC bugs all the time, Finish reading this
mail, choose an RC bug and try to fix it!

What we're planning at the moment are three (or more) weekends with BSPs
at different locations in parallel, each time with a different emphasis:

BSP on weekend 2008-03-07 to 2008-03-09
---------------------------------------
 + Work on qa-ftbfs bugs [8]
 + Do upgrade test and report/fix bugs

BSP on weekend 2008-04-04 to 2008-04-06
---------------------------------------
 + d-i and installation process
 + debian doc (release notes, upgrade notes)
 + Do upgrade test and report/fix bugs

BSP on weekend 2008-05-01 to 2008-05-04
---------------------------------------
 + Desktop related (gnome, kde, xfce, ...)
 + general RC bug fixing

At the moment, we still haven't found hosts for all real life BSPs - so
please check out if you can organize something. Please use [6] for
coordination! There is also a small howto [7] with facts to think about
before organizing a real life BSP.


Removal rules
~~~~~~~~~~~~~
To reduce the number of release critical bugs affecting testing, Marc
Brockschmidt has started to remove RC buggy packages without reverse
dependencies from testing. To give you a better impression if your
package is in direct danger of being removed, we have decided to set up
some rules for removals:
 Packages will get removed when a release critical bug has been filed
 more than 4 weeks ago and the bug log shows no movement towards
 resolving it. The latter means that at every time a message not
 older than two weeks should report the current status and point out
 what blocks fixing the problem.

At this time, only packages without r-dependencies will be removed.
Later on, nearer to the actual release date, we will consider removing
rc-buggy packages AND the packages' rdepends. Please run `rc-alert' from
the devscripts package to check which installed packages are in need
of your help.

Release goal review
~~~~~~~~~~~~~~~~~~~
Starting with this release update, we will report about the state
of our release goals, pointing out which need more help and which
have been finished:

* Drop debmake from Debian

Most of the packages still needing debmake to build have recently been
removed and the rest has been NMUed. debmake has been removed from unstable
and will be removed from testing.

* UTF-8 debian/changelog and debian/control

Only 40 packages still use obsolete charsets in their changelog or
control files. Please check if your package is listed on the lintian
tag pages:
http://lintian.debian.org/reports/Tdebian-changelog-file-uses-obsolete-national-encoding.html
http://lintian.debian.org/reports/Tdebian-control-file-uses-obsolete-national-encoding.html

Bugs against the affected packages will be filed soon.

* Switch /bin/sh to dash

Many issues with using dash as /bin/sh have been fixed in the past few
weeks, only a few are left open. We hope that all of them will be fixed
at the time of the next release update. Please help:
http://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=goal-dash;users=debian-release@lists.debian.org

Tricks from the Release Team
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
This paragraph is meant to be in every upcoming release update and
will contain one or more 'tricks'. Feel free to ask one of the release
team members for more information.

Executing 'grep-excuses <pkg>' will tell you why <pkg> has not
transitioned to testing yet. This can be helpful to fix any issues
which are blocking the transition or to know if it's wise to upload a
new version of a package or not. Note that interrupting mass
transitions by uploading a new version of a package in the middle of
a current transition is not really appreciated.

Cheers,
Marc
-- 
http://release.debian.org
Debian Release Team

Footnotes:
[0]  http://release.debian.org/lenny-goals.txt
[1]  http://lists.debian.org/debian-devel-announce/2007/07/msg00013.html
[2]  http://lists.debian.org/debian-alpha/2007/09/msg00072.html and similar
[3]  http://wiki.debian.org/CategoryLennyReleaseRecertification
[4]  http://lists.debian.org/debian-devel-announce/2007/09/msg00000.html
[5]  http://www.gunk.ca/products/automotive/images/BUG33C.jpg
[6]  http://wiki.debian.org/BSPMarathon
[7]  http://wiki.debian.org/HostingBSP
[8]  http://bugs.debian.org/cgi-bin/pkgreport.cgi?usertag=debian-qa@lists.debian.org
