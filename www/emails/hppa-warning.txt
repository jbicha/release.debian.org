To: "HPPA porters" <debian-hppa@lists.debian.org>, Debian Release <debian-release@lists.debian.org>
Subject: hppa in danger of being ignored for testing migration and eventual removal

Hi,

There appeared to be some porting progress right after the release, but that
seems to have stalled. The port is having trouble keeping up with building
and has some major flaws.

HPPA is clearly the worst at keeping up among the release architectures[0],
especially considering that mipsel has some temporary issues that are being
worked on. This is causing large problems with migrations.

With this in mind, and to ensure that everyone knows where they stand with
regards to HPPA and squeeze, clarification on the following would be
appreciated.

* What is the status of the different porting efforts?

* Has progress been made regarding the thread library migration?

* Has progress been made regarding proper java support?

* The machines that host the buildds still seem to have a very unreliable
  kernel. Is there any update on this?

* The debian-installer dailies that are now built again, but seem to fail to
  build most of the time. Is there any particular reason for this?


Please make sure to get satisfying answers to all of the above questions in the
next couple of weeks (by the 11th of May) or we will as a first step ignore
hppa for testing migration purposes. If you need some more time, please let us
know, but we're keen to ensure that this doesn't drag on too long :)

Cheers

Luk

[0] https://buildd.debian.org/stats/graph-week-big.png
