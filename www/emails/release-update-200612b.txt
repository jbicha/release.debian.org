Subject: release update - main blockers for release

Hi,

we wish those of you who celebrate it a Merry Christmas, and those of
you who change year soon a Happy New Year.  Release status of etch is not
as far as we all wanted at this time of the year, but - it is moving
forward!


There are a few items to resolve so that we could actually release.  We
need your help there - in whichever area you want to help us, please do
so.  Please remember: Releasing of Debian is a common effort of the
whole community.


There are also good news.  One of them is that the security team told us
that we now have security support for Etch (and also that Etch has been
in a good status for some time now regarding security).


linux-2.6
~~~~~~~~~
There are a few disturbing bug reports currently open.

A very bad one is file system corruption since the msync-patch was applied
(which was considered a pre-condition for LSB 3.1-qualification however) -
we share this issue with upstream linux kernel 2.6.19, and is not sorted
out there either (or rather, Martin Michlmayr is sorting it out both inside
the Debian kernel and upstream).

Also, there are quite many bug reports against linux-2.6 which need review.


Release Critical Bugs
~~~~~~~~~~~~~~~~~~~~~
Since the last release update, many release critical bug reports were
fixed, however also many filed - the number of release critical bugs
didn't really go down. More work needs to happen on resolving the number
of release critical bugs.  If the current trend continues, we can
release soon - but if more people would fix bad bugs, we could release
earlier.

A lot of release critical bug reports have been mass-filed during the
recent days on rather trivial issues - we decided to set the number
of minimum waiting days in our NMU policy to 5 days now, but of course
you can still wait longer if you consider this appropriate. We also
decided to not consider new detected violations against "Packages must
not install programs in the default PATH with different functionality
with the same file name" release critical for etch anymore - of course,
any such issue will be release critical for lenny (and please continue
fixing such issues).


Documentation: Release Notes, Installation Manual
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The documentation needs to be finalised for Etch.  This are mainly the
Release Notes and the Installation Manual.  We need also to figure out
the best order for updates of an installed system, and how regular users
can prevent important parts of their system to be removed when using 
aptitude on the command line.


Debian-Installer Release Candidate 2
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The last but not least requirement is the publication of the Debian-
Installer Release Candidate 2. 


libpng
~~~~~~
A package that gave us shivers is libpng.  Since the upload of a new 
upstream version, we noticed some regressions.  On deeper analysis,
we also detected missing symbols since Sarge - however, they even
disappeared earlier.  We finally arrived at a solution - that costed
many hours of our time, but the new version has already moved to testing
since then, so no more worries here.



Please allow us to remind you once again on the upload policy: If your
upload is not meant for Etch, do not upload it to unstable but to
experimental.  If your upload is meant for Etch, please make sure prior to
your upload that it matches our criteria as mentioned in the last mail,
and also make sure your upload actually reaches Etch.



Cheers,
-- 
Andi
Debian Release Team
