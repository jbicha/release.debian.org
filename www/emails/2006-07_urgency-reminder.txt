X-Modeline: vim:set ft=mail:
X-Status: DRAFT
To: debian-devel-announce@lists.debian.org
Subject: Reminder about proper upload urgency

Hi,

The latest release update [1] included a brief line about what the
preferred urgency was for uploads fixing RC bugs. Since then, though,
we've observed that a big percentage of RC bugs do still get fixed with
low-urgency uploads. So:

  ==================================================================
    For uploads that fix RC bugs present _in testing_, please use:

      - urgency=high if it's a security bug
      - urgency=medium for other RC bugs, including FTBFS

    Since these uploads will get less days in unstable, please
    prepare them with care, and do some extra testing yourself
    before uploading. This applies both to maintainer uploads,
    and NMUs.

    Also, please don't include extra, potentially disruptive
    changes in the same upload. Small adjustments are ok, but if
    the bug can't be fixed without introducing a new upstream
    version or some other invasive changes, then urgency=low is
    preferable.
  ==================================================================

It also concerns us that, sometimes, an RC bug will get reported against
the version in unstable, but it happens to affect the version in testing
as well. If when receiving or reading a bug, you happen to know this is
the case, please send the appropriate 'found <bug#> <testing-version>'
statement to control@bugs.debian.org.

Thanks for your cooperation!

References:

  [1] http://lists.debian.org/debian-devel-announce/2006/07/msg00005.html

-- 
NN
Debian Release Team
