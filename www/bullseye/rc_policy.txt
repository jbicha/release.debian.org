Release Critical Issues for bullseye
====================================

The purpose of this document is to be a correct, complete and canonical
list of issues that merit a "serious" bug under the clause "a severe
violation of Debian policy".

In addition to the issues listed in this document, an issue is release
critical if it:

    * makes unrelated software on the system (or the whole system)
      break
    * causes serious data loss
    * introduces a security hole on systems where you install the
      packages
      (these issues are "critical" severity)

    * makes the package in question unusable or mostly so
    * causes data loss
    * introduces a security hole allowing access to the accounts
      of users who use the package
      (these issues are "grave" severity)

    * in the maintainer's opinion, makes the package unsuitable
      for release
      (these issues are "serious" severity)

Further to this, certain issues may be exempted from being considered
release critical for bullseye by a release manager. This is expressed
by tagging the report "bullseye-ignore"; this should not be done without
explicit authorisation from a release manager.

Here's the list:

1. DFSG-freeness

    All content in main and contrib must meet the DFSG, both in .debs and
    in the source (including the .orig.tar.gz)
    Ref: 2.2.1 and 2.2.2

    Everything in non-free must be distributable by Debian.
    Ref: 2.3

    Every package must include a complete and verbatim copy of its
    copyright and distribution license in its "copyright" file. The
    Apache License, the Artistic License, the Creative Commons CC0-1.0
    license, the GFDL, the GPL, the LGPL and the Mozilla Public License
    should be replaced by references to the appropriate files in
    /usr/share/common-licenses. The copyright file must also include a
    pointer to upstream.
    Ref: 12.5

    The package changelog must be included.
    Ref: 12.7

2. Dependencies

    Packages in main cannot require any software outside of main
    for execution or compilation.
    Ref: 2.2.1

    Packages must include a "Depends:" line listing any other
    packages they require for operation, unless those packages are
    marked "Essential: yes". Packages must include a "Pre-Depends:"
    line listing any packages required by their preinst.
    Ref: 3.5

    If two packages cannot be installed together, one must list the other
    in its "Conflicts:" field.
    Ref: 7.4

    Essential packages must be (adequately) functional when unpacked
    but not installed.
    Ref: 3.8

    Packages listed in "Pre-Depends:" must be (adequately) functional
    when unpacked but not installed.
    Ref: RT?

    Shared library packages must include correct shlibs files or symbols
    files.
    Ref: 8.6

    Debconf .config scripts must only use tools present in essential
    packages.
    Ref: 6.?

    Packages must not install programs in the default PATH with
    different functionality with the same file name, even if they
    Conflict:.
    Ref: ?.?

3. Configuration files

    Packages must not modify their own or other packages conffiles
    programmatically. (The only correct way to modify a conffile is
    the user running an editor specifically; if anything more automated
    is required or useful, configuration files must _NOT_ be handled as
    conffiles)
    Ref: 10.7.3

    Conffiles must be plain text.
    Ref: dpkg?

    Packages must not modify other packages' configuration files
    except by an agreed upon API (eg, a /usr/sbin/update-foo command).
    Ref: 10.7.4

    Packages' /etc/init.d scripts must be treated as configuration files.
    Packages' /etc/default scripts must be treated as configuration files.
    Ref: 10.7.1

    Packages must not include files in /etc/rcN.d, but must instead use
    update-rc.d to indicate when their init scripts should be run.
    Ref: 9.3.1

    Packages that need to install a cron job, must place a script in
    /etc/cron.{daily,weekly,monthly}, or a file in /etc/cron.d. In either
    case the file must be treated as a configuration file.
    Ref: ? and 10.7.1

    All configuration files must reside in /etc.
    Ref: 10.7.2

    Changes to configuration files must be preserved during a package
    upgrade. Configurations must be preserved on package removal, and
    only deleted when the package is purged.
    Ref: 10.7.3

4. Autobuilding

    Packages must list any packages they require to build beyond those
    that are "build-essential" in the appropriate Build-Depends: fields.
    Ref: 4.2

    debian/rules must be an executable makefile, beginning with the
    line "#!/usr/bin/make -f" so that it can be invoked by running
    the makefile rather than invoking make explicitly.
    Ref: 4.9

    debian/rules must include the targets: clean, binary, binary-arch,
    binary-indep, build, build-arch and build-indep; and these targets
    cannot require any interaction with the user. The build target must
    not do anything that requires root privileges. These targets must
    not change the package's build-dependencies or the changelog.
    Ref: 4.9

    Packages must autobuild without failure on all architectures on
    which they are supported. Packages must be supported on as many
    architectures as is reasonably possible. Packages are assumed to
    be supported on all architectures for which they have previously
    built successfully. Prior builds for unsupported architectures
    must be removed from the archive (contact -release or ftpmaster
    if this is the case).
    Ref: RT?

    Packages must be buildable within the same release.
    Ref: RT?

5. General

  (a) Supportable

    Packages in the archive must not be so buggy or out of date that we
    refuse to support them.
    Ref: 2.2.1

  (b) Security

    Programs must be setup to use the minimum privileges they can. (ie,
    not setuid where setgid will suffice; not setuid root where setuid
    some other user will suffice; setuid root for the minimum period
    possible, etc)
    Ref: RT?

  (c) File hierarchy

    Packages must place all files in the locations specified by the
    FHS, with the clarification that architecture-independent
    supporting code may be stored in a subdirectory of /usr/lib
    instead of in a subdirectory of /usr/share.
    Ref: 9.1.1

    Any subdirectories created in the /usr/local hierarchy must be
    created and removed with mkdir and rmdir in postinst and prerm;
    and the scripts must not fail if those commands fail. Packages
    must not rely on the presence or absence of any files or
    directories under /usr/local.
    Ref: 9.1.2

  (d) UIDs

    Packages must only use system uids for the purposes for which they
    were allocated.
    Ref: 9.2.1

  (e) Keymap

    Packages must interpret keypresses and the keyboard layout
    in a consistent manner (in how delete and backspace operate
    for example).
    Ref: 9.8

  (f) Libraries

    Shared libraries must be compiled with -fPIC, and normally static
    libraries must not be. If you need to provide static libraries
    compiled with -fPIC, call it "<libname>_pic.a".
    Ref: 10.2

    Libraries must normally be compiled with -D_REENTRANT.
    Ref: 10.2

    Shared libraries must normally be linked with all libraries they
    use symbols from.
    Ref: 10.2

  (g) Scripts

    Scripts must include the appropriate #! line, and set executable.
    The package providing the script must Depends: on the appropriate
    package providing the interpreter.
    Ref: 10.4

  (h) Temporary files

    Any programs and scripts that create files in /tmp or other
    world writable directories must use a mechanism which fails if
    the file already exists.
    Ref: 10.4

  (i) Device files

    Packages must not include device files in the .deb, but should
    instead invoke MAKEDEV from postinst. Packages must not remove device
    files.
    Ref: 10.6 (not aligned with policy)

  (j) Log files

    Log files must be rotated, preferably using /etc/logrotate.d.
    Ref: 10.8

    Log files must be removed when the package is purged. (As such,
    they shouldn't fail if the package is removed-but-not-purged,
    or if the admin has deleted all the packges' logs -- check
    logrotate's "missingok" directive)
    Ref: 10.8 (policy says should)

  (k) Editors and Pagers

    Packages that need to launch an editor or pager must use the EDITOR
    or PAGER environment variables if set, or fall back to /usr/bin/editor
    or /usr/bin/pager.
    Ref: 11.4

  (l) Mail

    Mail programs must lock mailboxes in an NFS-safe way. MTAs must
    recreate mailboxes if needed.
    Ref: 11.6

    If setgid mail is used for locking, MUAs must avoid allowing
    access to other users' mail spools.
    Ref: 11.6

    MTAs must provide /usr/sbin/sendmail, and a symlink to that
    program as /usr/lib/sendmail.  The /usr/sbin/sendmail command must
    implement all command-line options specified by the LSB with the
    exception of the -bs switch.  MTAs which do not provide the -bs
    switch must Conflict: with the lsb package.
    Ref: 11.6 and ?

    MTAs must provide a newaliases program, that makes any changes
    to /etc/aliases effective.
    Ref: 11.6 (policy says it may do nothing)

    MTAs must spool local mail to /var/mail/<user> by default.
    Ref: ??

    MTAs must list the virtual package "mail-transport-agent" in their
    Provides: Conflicts: and Replaces: fields.
    Ref: 11.6

  (m) X support

    Packages that can be configured to support X, must be
    so configured, and must list the appropriate packages in
    Depends:. Similarly when packages can be configured to support
    gtk, gnome or kde. (Providing different versions of the package
    without X or gnome/kde support should only be done when there
    is a particular need)
    Ref: 11.8.1 and ??

  (n) Games

    Games may be setgid games in order to write to high score files, but
    must not be setuid.
    Ref: 11.11

  (o) Documentation

    Packages must have a useful extended description.
    Ref: 3.4

    Preformatted cat pages must not be installed.
    Ref: 12.1

    Packages must not require the existance of any files in /usr/share/doc
    in order to function.
    Ref: 12.3

  (p) Python

    Packages providing python modules must comply with the python
    policy (naming scheme and dependencies).
    Ref: https://www.debian.org/doc/packaging-manuals/python-policy/

6. Quality Assurance checks

  (a) autopkgtest

    Package are encouraged to implement autopkgtests. These tests must
    test at least one of its own installed binary packages in some
    non-trivial way, or must be marked as superficial. Examples of
    superficial tests include calling executables with --version or
    --help, testing for existence of files in binary packages, and
    $(python3 -c "import foo"). Failing tests on amd64 and arm64 are
    RC. Test regressions on release architectures are RC.
    Ref: RT

    Packages must not cause regressions in autopkgtests of other packages
    on any release architecture.
    Ref: RT

  (b) buildd

    Packages must be built on a buildd.
    Ref: RT

  (c) piuparts

    Packages must not regress in piuparts installability in testing.
    Ref: RT
